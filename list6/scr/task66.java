package scr;

import java.io.PrintWriter;
import java.util.Scanner;
class task66 {
    public static void main(String[] args) {
        try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)){
            String lex =  "qwertyuiopasdfghjklzxcvbnm";
            int index = lex.indexOf(in.next());
            char ans = index==lex.length()-1? lex.charAt(0) : lex.charAt(index+1);
            out.print(ans);
        }
    }
}
