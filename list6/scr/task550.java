package scr;

import java.io.*;
import java.text.*;
import java.util.*;

public class task550 {
    public static void main(String[] args) {
        try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
            String year = in.next();
            int days = 255;
            String date = "01/01/" + year;
            SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
            Calendar cal = Calendar.getInstance();
            try {
                cal.setTime(dateFormat.parse(date));
            } catch (ParseException e) {
                e.printStackTrace();
            }
            cal.add(Calendar.DATE, days);
            date = dateFormat.format(cal.getTime());
            out.print(date);
        }
    }
}
