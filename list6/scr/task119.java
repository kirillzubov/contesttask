package scr;

import java.io.*;
import java.util.*;

public class task119 implements Comparable<task119> {
    int year;
    int month;
    int hour;

    task119(int year, int month, int hour) {
        this.year = year;
        this.month = month;
        this.hour = hour;
    }

    @Override
    public String toString() {
        return year + " " + month + " " + hour;
    }

    @Override
    public int compareTo(task119 o) {
        if (this.year > o.year) {
            return 1;
        } else if (this.year < o.year) {
            return -1;
        } else {
            if (this.month > o.month) {
                return 1;
            } else if (this.month < o.month) {
                return -1;
            } else {
                if (this.hour > o.hour) {
                    return 1;
                } else if (this.hour < o.hour) {
                    return -1;
                } else {
                    return 0;
                }
            }
        }
    }

    public static void main(String[] args) {
        try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
            int n = in.nextInt();
            in.nextLine();
            task119[] mass = new task119[n];
            for (int i = 0; i < n; i++) {
                mass[i] = new task119(in.nextInt(), in.nextInt(), in.nextInt());
                in.nextLine();
            }
            Arrays.sort(mass);
            for (int i = 0; i < n; i++) {
                out.println(mass[i].toString());
            }
        }
    }
}
