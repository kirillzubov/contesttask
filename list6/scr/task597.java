package scr;

import java.io.PrintWriter;
import java.util.Scanner;

class task597 {
    public static void main(String[] args) {
        try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
            String ans = in.nextInt() >= in.nextInt() + in.nextInt() ? "YES" : "NO";
            out.print(ans);
        }
    }
}
