package scr;

import java.io.PrintWriter;
import java.util.*;

import static java.lang.Math.pow;

public class task26 {
    public static void main(String[] args) {
        try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
            int x1 = in.nextInt();
            int y1 = in.nextInt();
            int r1 = in.nextInt();
            int x2 = in.nextInt();
            int y2 = in.nextInt();
            int r2 = in.nextInt();

            int d = (int) (pow(x1 - x2, 2) + pow(y1 - y2, 2));
            if (r1 > d || r2 > d) {
                if (d >= (int) pow(r1 - r2, 2)) out.print("YES");
                else out.print("NO");
            }else {
                if (d <= (int) pow(r1 + r2, 2)) out.print("YES");
                else out.print("NO");
            }
        }
    }
}
