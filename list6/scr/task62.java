package scr;

import java.util.*;
public class task62 {
    public static void main(String[] args) {
        char[] inp = new Scanner(System.in).next().toCharArray();
        System.out.print((inp[0] - 66 + inp[1]) % 2 == 0 ? "BLACK" : "WHITE");
    }
}
