package scr;

import java.util.*;
public class task148 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in); {
            int a = in.nextInt();
            int b = in.nextInt();
            while (a!=0){
                b=b%a;
                b^=a;
                a^=b;
                b^=a;
            }
            System.out.print(b);
        }
    }
}
