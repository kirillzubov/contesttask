package scr;

import java.io.PrintWriter;
import java.util.Scanner;
class t {
    public static void main(String[] args) {
        try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
            int ans = (in.nextInt() + in.nextInt()) * in.nextInt() << 1;
            out.print(ans / 16 + (ans % 16 == 0 ? 0 : 1)); }}}
