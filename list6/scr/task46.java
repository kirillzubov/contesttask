package scr;

import java.io.*;
import java.math.*;
import java.util.*;
class task46 {
    public static void main(String[] args) {
        try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
            out.print(new BigDecimal("2.7182818284590452353602875").round(new MathContext(in.nextInt()+1)));
        }
    }
}
