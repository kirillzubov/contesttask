package scr;

import java.io.*;
import java.util.*;

class task550n {
    public static void main(String[] a) {
        int y = new Scanner(System.in).nextInt();
        new PrintWriter(System.out).format("%02d/%02d/%04d", (y % 400 == 0) | (y % 4 == 0 && y % 100 != 0) ? 12 : 13, 9, y).flush();
    }
}
