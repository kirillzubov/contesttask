import java.io.PrintWriter;
import java.util.Scanner;


public class task688 {
    public static void main(String[] args) {
        try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
            long[] gop = {in.nextLong(), in.nextLong()};
            long[] dog = {in.nextLong(), in.nextLong()};
            long n = in.nextLong();
            long[] hole = new long[2];
            for (int i = 1; i <= n; i++) {
                hole[0] = in.nextLong();
                hole[1] = in.nextLong();
                long dGop = (pow(gop[0] - hole[0]) + pow(gop[1] - hole[1])) * 4L;
                long dDog = pow(dog[0] - hole[0]) + pow(dog[1] - hole[1]);
                if (dGop <= dDog) {
                    out.print(i);
                    return;
                }
            }
            out.print("NO");
        }
    }

    static long pow(long x) {
        return x * x;
    }
}