import java.io.PrintWriter;
import java.util.Scanner;


public class task28 {
    public static void main(String[] args) {
        try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
            int[] cord = {in.nextInt(), in.nextInt(), in.nextInt(), in.nextInt()};
            int difx = cord[0] - cord[2];
            int dify = cord[1] - cord[3];

            int x = in.nextInt();
            int y = in.nextInt();
            x = difx == 0 ? -(x - 2*cord[0]) : x;
            y = dify == 0 ? -(y - 2*cord[1]) : y;

            out.print(x + " " + y);
        }
    }
}
