import java.util.*;
import java.util.stream.IntStream;
public class task496 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = Integer.parseInt(in.nextLine());
        List<String> list = new ArrayList<>();
        list.addAll(0, Arrays.asList(in.nextLine().split(" ")));
        list.add(0, list.get(n - 1));
        list.add(n + 1, list.get(1));
        int[] grad = list.stream().mapToInt(Integer::parseInt).toArray();
        System.out.print(IntStream.range(0, n).limit(n).map(i -> grad[i] + grad[i + 1] + grad[i + 2]).max().getAsInt());
    }
}
