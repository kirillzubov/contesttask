import java.io.*;
import java.util.*;
public class Task680 {
    public static void main(String[] a) {
        try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
            int n = in.nextInt();
            out.print(3L << n - 1);
        }
    }
}
