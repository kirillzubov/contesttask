import java.io.*;
import java.util.*;
public class Task676 {
    public static void main(String[] args) {
        try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
            if (in.nextInt()%3!=0) out.print(1);
            else out.print(2);
        }
    }
}
