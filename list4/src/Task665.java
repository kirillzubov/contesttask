import java.io.*;
import java.util.*;

public class Task665 {
    public static void main(String[] args) {
        try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
            Scanner ind = in.useDelimiter("\\s+|:");
            int hour = ind.nextInt();
            int min = ind.nextInt();
            while (true) {
                min = (min + 1) % 60;
                hour = (min == 0) ? (hour + 1) % 24 : hour;
                int nim = min / 10 + min % 10 * 10;
                if (hour == nim) {
                    out.printf("%02d:%02d", hour, min);
                    return;
                }
            }
        }
    }
}