import java.io.*;
import java.util.*;

public class Task67 {
    private static int[] parserIp4(String input) {
        return Arrays.stream(input.split("\\.")).mapToInt(Integer::parseInt).toArray();
    }

    public static void main(String[] args) {
        try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
            int n = in.nextInt();
            int[][] mask = new int[n][];
            for (int i = 0; i < n; i++) {
                mask[i] = parserIp4(in.next());
            }
            int m = in.nextInt();
            int[][] ips1 = new int[m][];
            int[][] ips2 = new int[m][];

            for (int i = 0; i < m; i++) {
                ips1[i] = parserIp4(in.next());
                ips2[i] = parserIp4(in.next());
            }
            for (int i = 0; i < m; i++) {
                int count = 0;
                for (int k = 0; k < n; k++) {
                    boolean belongs = true;
                    for (int j = 0; j < 4; j++) {
                        belongs = belongs && (ips1[i][j] & mask[k][j]) == (ips2[i][j] & mask[k][j]);
                    }
                    count = belongs ? count + 1 : count;
                }
                out.println(count);
            }
        }
    }
}