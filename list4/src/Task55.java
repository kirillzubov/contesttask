import java.io.*;
import java.util.*;

public class Task55 {
    private static double findTwoCirclesArea(double x1, double y1, double x2, double y2, double r) {
        double distance = Math.sqrt((x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2));
        double intersectionArea;
        if (x1 == x2 && y1 == y2) {
            return Math.PI * r * r;
        } else if (distance >= 2 * r) {
            intersectionArea = 0;
        } else {
            double f = 2 * Math.acos(distance / (2 * r));
            double s = (r * r * (f - Math.sin(f)));
            intersectionArea = s;
        }
        return 2 * Math.PI * r * r - intersectionArea;
    }

    public static void main(String[] args) {
        try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
            double x1 = in.nextDouble(); double y1 = in.nextDouble();
            double x2 = in.nextDouble(); double y2 = in.nextDouble();
            double r = in.nextDouble();
            double s = in.nextDouble();

            if (findTwoCirclesArea(x1, y1, x2, y2, r) > s) {
                out.print("YES");
            } else {
                out.print("NO");
            }
        }
    }
}
