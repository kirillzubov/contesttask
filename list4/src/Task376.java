import java.io.*;
import java.util.*;

public class Task376 {
    private static int getDaysCount(int birthDay, int birthMonth, int day, int month, int year) {
        int[] daysInMonth = {0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
        int daysCount = 0;
        while (true) {
            if ((year % 400 == 0) || ((year % 4 == 0) && (year % 100 != 0))) {
                daysInMonth[2] = 29;
            } else {
                daysInMonth[2] = 28;
            }
            while (month <= 12) {
                while (day <= daysInMonth[month]) {
                    if (day == birthDay && month == birthMonth) {
                        return daysCount;
                    }
                    daysCount++;
                    day++;
                }
                day = 1;
                month++;
            }
            year++;
            month = 1;
        }
    }

    public static void main(String[] args) {
        try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
            int birthDay = in.nextInt();
            int birthMonth =in.nextInt();
            int day  =in.nextInt();
            int month = in.nextInt();
            int year = in.nextInt();
            out.println(getDaysCount(birthDay, birthMonth, day, month, year));
        }
    }
}

