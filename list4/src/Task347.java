import java.io.*;
import java.util.*;

public class Task347 {
    private static boolean isStraight(int[] cards) {
        int straightCount = 0;
        for (int card : cards) {
            if (card != 0) {
                straightCount++;
            } else {
                straightCount = 0;
            }
            if (straightCount == 5) {
                return true;
            }
        }
        return false;
    }

    public static void main(String[] args) {
        try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
            int[] cards = new int[14];
            for (int i = 0; i < 5; i++) {
                cards[in.nextInt()]++;
            }
            int m1 = Integer.MIN_VALUE;
            int m2 = Integer.MIN_VALUE;
            int j = 0;
            for (int i = 0; i < 14; i++) {
                if (cards[i] > m1) {
                    m1 = cards[i];
                    j = i;
                }
            }
            for (int i = 0; i < 14; i++) {
                m2 = cards[i] > m2 & i != j ? cards[i] : m2;
            }
            if (m1 == 5) {
                out.print("Impossible");
            } else if (m1 == 4) {
                out.print("Four of a Kind");
            } else if (m1 == 3 && m2 == 2) {
                out.print("Full House");
            } else if (isStraight(cards)) {
                out.print("Straight");
            } else if (m1 == 3) {
                out.print("Three of a Kind");
            } else if (m1 == 2 && m2 == 2) {
                out.print("Two Pairs");
            } else if (m1 == 2) {
                out.print("One Pair");
            } else {
                out.print("Nothing");
            }
        }
    }
}