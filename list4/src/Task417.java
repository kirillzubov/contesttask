import java.io.*;
import java.text.*;
import java.util.*;

public class Task417 {
    enum DayOfTheWeek {
        Monday, Tuesday, Wednesday, Thursday, Friday, Saturday, Sunday
    }

    private static String getData(int days) {
        DayOfTheWeek[] daysOfWeek = DayOfTheWeek.values();
        String dayOfWeek = String.valueOf(daysOfWeek[(days) % 7]);
        int[] daysInMonth = {0, 31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
        if (days > 366) {
            days -= 366;
            if (days > 365) {
                days -= 365;
            }
            daysInMonth[2] = 28;
        }
        int month = 1;
        while (true) {
            if (days <= daysInMonth[month]) {
                return String.format("%s, %02d.%02d", dayOfWeek, days, month);
            }
            days -= daysInMonth[month];
            month++;
        }
    }

    public static void main(String[] args) {
        try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
            int days = in.nextInt();
            out.println(getData(days + 1));
        }
    }
}
