import java.io.*;
import java.util.*;

public class Task385 {
    static class Point {
        final int x;
        final int y;

        Point(int x, int y) {
            this.x = x;
            this.y = y;
        }
    }

    public static void main(String[] args) {
        try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
            int n = in.nextInt();
            Point[] points = new Point[n];

            for (int i = 0; i < n; i++) {
                points[i] = new Point(in.nextInt(), in.nextInt());
            }
            Set<Double> dists = new TreeSet<>();
            for (int i = 0; i < n; i++) {
                for (int j = i + 1; j < n; j++) {
                    int distX = points[i].x - points[j].x;
                    int distY = points[i].y - points[j].y;
                    dists.add(Math.sqrt(distX * distX + distY * distY));
                }
            }
            out.println(dists.size());
            for (double d : dists) {
                out.println(d);
            }
        }
    }
}
