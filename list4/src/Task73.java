import java.io.*;
import java.util.*;

public class Task73 {
    private static char getNum(char c, int i) {
        int ans;
        if ('0' <= c && c <= '9') {
            ans = c - '0';
        } else {
            ans = c - 'A' + 10;
        }
        ans = ((ans - i) % 27 + 27) % 27;
        return ans == 0 ? ' ' : (char) (ans - 1 + 'a');
    }

    public static void main(String[] args) {
        try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
            String str = in.next();
            for (int i = 0; i < str.length(); i++) {
                out.print((getNum(str.charAt(i), i + 1)));
            }
        }
    }
}