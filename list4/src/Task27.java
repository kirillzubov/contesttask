import java.io.*;
import java.util.*;

public class Task27 {
    public static void main(String[] args) {
        try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
            int w = in.nextInt();
            int h = in.nextInt();
            int n = in.nextInt();
            int[][] square = new int[h][w];

            for (int i = 0; i < n; i++) {
                int x1 = in.nextInt();
                int y1 = in.nextInt();
                int x2 = in.nextInt();
                int y2 = in.nextInt();
                for (int y = y1; y < y2; y++) {
                    for (int x = x1; x < x2; x++) {
                        square[y][x] = 1;
                    }
                }
            }
            int numOfFillRect = 0;
            for (int i = 0; i < h; i++) {
                for (int j = 0; j < w; j++) {
                    numOfFillRect += square[i][j];
                }
            }
            int numOfNonFillRect = h * w - numOfFillRect;
            out.println(numOfNonFillRect);

        }
    }
}
