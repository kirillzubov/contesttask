import java.io.*;
import java.util.*;
public class Task411 {
    public static void main(String[] args) {
        try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
            int a = in.nextInt();
            int b = in.nextInt();
            int c = in.nextInt();
            if (a == 0 && b == 0 && c == 0) {
                out.print(-1);
                return;
            }
            if (a == 0 && b == 0) {
                out.print(0);
                return;
            }
            if (a == 0) {
                out.println(1);
                out.println(-1.0 * c / b);
                return;
            }
            double d = b * b - 4.0 * a * c;
            if (d < 0) {
                out.print(0);
            } else if (d == 0) {
                out.println(1);
                out.print(-b / (2.0 * a));

            } else {
                out.println(2);
                out.println((-b - Math.sqrt(d)) / (2.0 * a));
                out.print((-b + Math.sqrt(d)) / (2.0 * a));
            }
        }
    }
}
