import java.io.*;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Task80 {
    public static void main(String[] args) {
        try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
            try {
                String pattern = "\\A([\\-\\+]?\\d+)([\\+\\-\\*\\/])([\\-\\+]?\\d+)\\=([\\-\\+]?\\d+)\\Z";
                String input = in.next(pattern);
                Pattern p = Pattern.compile(pattern);
                Matcher m = p.matcher(input);
                if (m.find()) {
                    String sign = m.group(2);
                    double a = Double.valueOf(m.group(1));
                    double b = Double.valueOf(m.group(3));
                    double ans = Double.valueOf(m.group(4));
                    double realAns = 0;
                    switch (sign) {
                        case "+":
                            realAns = a + b;
                            break;
                        case "-":
                            realAns = a - b;
                            break;
                        case "*":
                            realAns = a * b;
                            break;
                        case "/":
                            realAns = a / b;
                            break;
                    }
                    if (realAns == ans) {
                        out.print("YES");
                    } else {
                        out.print("NO");
                    }
                }
            } catch (Exception e) {
                out.print("ERROR");
            }
        }
    }
}
