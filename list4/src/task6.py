def multi(list):
    r = 1
    for x in list:
        r *= x
    return r

n = int( input())
if(n==1):
    print(1)
else:
    num = []
    num.append(n)
    num.append(2)
    while(len(num)!=n):
        num.append(1)
    if(not(sum(num)==2*n and multi(num)==2*n) and sum(num)<1000):
        print(-1)
    else:
        for i in num:
            print(i, end=" ")