import java.io.*;
import java.util.*;

public class Task570 {
    private static boolean isCircle(Point a, Point b, boolean[][] screen) {
        for (int i = a.y; i <= b.y; i++) {
            for (int j = a.x; j <= b.x; j++) {
                if (!screen[i][j]) {
                    return true;
                }
            }
        }
        return false;
    }

    private static class Point {
        int x;
        int y;

        Point(int x, int y) {
            this.x = x;
            this.y = y;
        }
    }

    public static void main(String[] args) {
        try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
            int n = in.nextInt();
            int m = in.nextInt();
            Point a = new Point(n - 1, m - 1);
            Point b = new Point(0, 0);
            boolean[][] screen = new boolean[n][m];

            for (int i = 0; i < n; i++) {
                String s = in.next();

                for (int j = 0; j < m; j++) {
                    if (s.charAt(j) == '*') {
                        screen[i][j] = true;
                        a.x = Math.min(a.x, j);
                        a.y = Math.min(a.y, i);
                        b.x = Math.max(b.x, j);
                        b.y = Math.max(b.y, i);
                    } else {
                        screen[i][j] = false;
                    }
                }
            }
            if (b.y - a.y < 0 || Math.abs((b.y - a.y) - (b.x - a.x)) > 4) {
                out.print("CIRCLE");
                return;
            }
            int diff = (b.y - a.y) - (b.x - a.x);
            for (int i = 0; i < 2; i++) {
                if (diff > 0) {
                    if (diff % 2 == 0) {
                        a.x--;
                        b.x++;
                    } else {
                        b.x++;
                    }
                } else if (diff < 0){
                    if (diff % 2 == 0) {
                        a.y--;
                        b.y++;
                    } else {
                        b.y++;
                    }
                }
            }
            if (b.y - a.y >= 2) {
                a.y++;
                b.y--;
                if (b.y - a.y >= 2) {
                    a.y++;
                    b.y--;
                }
            } else if (a.x < 1 || a.y < 1 || b.x > n - 2 || b.y > m - 2) {
                out.print("CIRCLE");
                return;
            }
            if (b.x - a.x >= 2) {
                a.x++;
                b.x--;
                if (b.x - a.x >= 2) {
                    a.x++;
                    b.x--;
                }
            }else if (a.x < 1 || a.y < 1 || b.x > n - 2 || b.y > m - 2) {
                out.print("CIRCLE");
                return;
            }
            if (isCircle(a, b, screen)) {
                out.print("CIRCLE");
                return;
            }
            out.print("SQUARE");
        }
    }
}