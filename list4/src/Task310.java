import java.io.*;
import java.util.*;

public class Task310 {
    public static void main(String[] args) {
        try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
            int n = in.nextInt();
            for (int i = 0; i < n; i++) {
                long x = in.nextInt();
                long y = in.nextInt();
                long a = in.nextInt();
                if((x % a == 0 && a==2 && (y - 1) % a == 0) ||
                        (y % a == 0 && a==2 && (x - 1) % a == 0) ||
                        ((x - 1) % a == 0 && (y - 1) % a == 0) ||
                        (x % a == 0 && (y - 2) % a == 0) ||
                        (y % a == 0 && (x - 2) % a == 0)) {
                    out.print(1);
                } else {
                    out.print(0);
                }
            }
        }
    }
}

