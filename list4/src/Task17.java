import java.io.*;
import java.util.*;

public class Task17 {
    private static int getLastIndexOfPrefixFunction(int[] s) {
        int[] p = new int[s.length - 1];
        for (int i = 1; i < s.length - 1; i++) {
            int k = p[i - 1];
            while (k > 0 && s[i] != s[k]) {
                k = p[k - 1];
            }
            if (s[i] == s[k]) {
                k++;
            }
            p[i] = k;
        }
        return p[p.length - 1];
    }

    public static void main(String[] args) {
        try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
            int n = in.nextInt();
            int[] nums = new int[n];
            for (int i = 0; i < n; i++) {
                nums[i] = in.nextInt();
            }
            int p = getLastIndexOfPrefixFunction(nums);
            if ((n-1)%(n - p - 1)!=0) {
                out.print(n - 1);
                return;
            }
            out.print(n - p - 1);
        }
    }
}
