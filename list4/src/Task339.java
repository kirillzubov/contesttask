import java.io.*;
import java.util.*;

public class Task339 {
    private static int[] convert(String[] str) {
        int[] intAr = new int[str.length];
        for (int i = 0; i < str.length; i++) {
            intAr[i] = Integer.parseInt(str[i]);
        }
        return intAr;
    }

    private static int getCountDays(int day1, int month1, int year1, int day2, int month2, int year2) {
        int[] daysInMonth = {0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
        int days = 0;
        while (true) {
            if ((year1 % 400 == 0) || ((year1 % 4 == 0) && (year1 % 100 != 0))) {
                daysInMonth[2] = 29;
            } else {
                daysInMonth[2] = 28;
            }
            while (month1 <= 12) {
                if (year1 == year2 && month1 == month2) {
                    days += day2-day1+1;
                    return days;
                }

                days += daysInMonth[month1]-day1+1;
                day1 = 1;
                month1++;
            }
            year1++;
            month1 = 1;
        }
    }

    public static void main(String[] args) {
        try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
            int[] data1 = convert(in.next().split("\\."));
            int[] data2 = convert(in.next().split("\\."));
            out.println(getCountDays(data1[0], data1[1], data1[2], data2[0], data2[1], data2[2]));
        }
    }
}
