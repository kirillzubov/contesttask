import java.io.*;
import java.util.*;

public class Task602 {
    public static void main(String[] args) {
        try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
            int n = in.nextInt();
            int ans1 = 0;
            int ans2 = 0;
            int min = Integer.MAX_VALUE;
            int prev = in.nextInt();
            for (int i = 1; i < n; i++) {
                int curr = in.nextInt();
                if (min > curr - prev) {
                    ans1 = prev;
                    ans2 = curr;
                    min = curr - prev;
                }
                prev = curr;
            }
            out.println(ans1 + " " + ans2);
        }
    }
}
