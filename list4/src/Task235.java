import java.io.*;
import java.util.*;

public class Task235 {
    private static int dir = 0;
    private static int x = 50;
    private static int y = 50;
    private static int countStep = 0;
    private static int[][] cords = new int[101][101];

    private static void step(char com) {
        if (com == 'S') {
            cords[x][y] += 1;
            countStep++;
            if (dir == 0) {
                x += 1;
            } else if (dir == 1) {
                y += 1;
            } else if (dir == 2) {
                x -= 1;
            } else if (dir == 3) {
                y -= 1;
            } else {
                throw new Error();
            }
        } else if (com == 'L') {
            dir = (dir + 3) % 4;
        } else if (com == 'R') {
            dir = (dir + 1) % 4;
        } else {
            throw new Error();
        }
    }

    public static void main(String[] args) {
        try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
            char[] command = in.nextLine().toCharArray();
            for (char comm : command) {
                step(comm);
                if (cords[x][y] == 1) {
                    out.println(countStep);
                    return;
                }
            }
            out.println(-1);
        }
    }
}
