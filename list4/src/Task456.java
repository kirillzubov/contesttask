import java.io.*;
import java.util.*;

public class Task456 {
    public static void main(String[] args) {
        try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
            int x = in.nextInt();
            int y = in.nextInt();
            int fib1 = 0;
            int fib2 = 1;
            for (int k = 1; k < x; k++) {
                int c = fib2;
                fib2 = fib1 + fib2;
                fib1 = c;
            }
            int i = 1;
            while (i<=y){
                int j = 1;
                while (j<=i){
                    if (fib2*i-fib1*j==y){
                        if (i-j>=0) {
                            out.println(i + " " + (i - j));
                            return;
                        }
                    }
                    j++;
                }
                i++;
            }
        }
    }
}
