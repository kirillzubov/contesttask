import java.io.*;
import java.util.*;

public class Task444 {
    public static void main(String[] args) {
        try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
            int n = in.nextInt();
            Set<Integer> pages = new TreeSet<>();
            for (int i = 0; i < n; i++) {
                pages.add(in.nextInt());
            }
            Iterator<Integer> it = pages.iterator();
            StringBuilder sb = new StringBuilder();
            StringBuilder littleSb = new StringBuilder();
            int last = it.next();
            if (!it.hasNext()) {
                out.print(last);
                return;
            }
            sb.append(last + ", ");
            while (true) {
                int curr = it.next();
                if (!it.hasNext()&&last + 1 != curr) {
                    String str = littleSb.length()>= 5 ? "..., " + last + ", " : littleSb.toString();
                    sb.append(str);
                    littleSb.setLength(0);
                    littleSb.append(curr);
                    sb.append(littleSb);
                    break;
                }
                if (!it.hasNext()) {
                    String str = littleSb.length() >= 5 ? "..., " : littleSb.toString();
                    sb.append(str);
                    littleSb.setLength(0);
                    littleSb.append(curr);
                    sb.append(littleSb);
                    break;
                }
                if (last + 1 != curr) {
                    String str = littleSb.length()-String.valueOf(last).length()-2 >= 5 ? "..., " + last + ", " : littleSb.toString();
                    sb.append(str);
                    littleSb.setLength(0);
                    sb.append(curr + ", ");
                }else {
                    littleSb.append(curr + ", ");
                }
                last = curr;
            }
            out.print(sb);
        }
    }
}