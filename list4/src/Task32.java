import java.io.*;
import java.util.*;

public class Task32 {

    private static int getMin(String input) {
        StringBuilder sb = new StringBuilder();
        if (input.charAt(0) == '-') {
            return -getMax(input.substring(1));
        }
        char[] ch = input.toCharArray();
        Arrays.sort(ch);
        sb.append(ch);
        if (sb.charAt(0) == '0') {
            for (int i = 0; i < sb.length(); i++) {
                if (sb.charAt(i) != '0') {
                    sb.insert(0, sb.charAt(i));
                    sb.deleteCharAt(i + 1);
                    break;
                }
            }
        }
        return Integer.valueOf(sb.toString());
    }

    private static int getMax(String input) {
        StringBuilder sb = new StringBuilder();
        if (input.charAt(0) == '-') {
            return -getMax(input.substring(1));
        }
        char[] ch = input.toCharArray();
        Arrays.sort(ch);
        sb.append(ch);
        sb.reverse();
        return Integer.valueOf(sb.toString());
    }

    public static void main(String[] args) {
        try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
            int a = getMax(in.next());
            int b = getMin(in.next());
            out.println(a - b);
        }
    }
}