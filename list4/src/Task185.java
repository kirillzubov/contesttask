import java.io.*;
import java.util.*;

public class Task185 {
    public static void main(String[] args) {
        try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
            int n = in.nextInt();int k = in.nextInt();
            boolean[] lost = new boolean[n + 1];
            lost[k] = true;
            while (in.nextInt() != 0) {
                lost[in.nextInt()]= true;
            }
            for (int i = 1; i <= n; i++) {
                if (!lost[i]) {
                    out.print("No");
                    return;
                }
            }
            out.print("Yes");
        }
    }
}