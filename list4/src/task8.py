import math

def step(i, j, x):
    if (x >= a):
        return False
    for i in way[i]:
        flags[i] = True
        x += 1
        if (i == j):
            u[0] += 1
            break
        if (flags == [True] * n and u[0] == len((way[0]))):
            return True
        step(i, j, x)

n = int(input())
a = round(math.log2(n) + 1)
m = []
e = []
ma = 0

if (n == 1):
    print(input())
else:
    for j in range(n):
        l = list(map(int, input().split()))
        l.pop(j)
        m.append(l)
        mi = min(l)
        ma = max(ma, mi)
        e.extend(l)
    e = (list(set(e)))
    e.sort()
    while (e[0] < ma):
        e.pop(0)
    fl = False
    flags = [False] * n
    way = [set() for _ in range(n)]
    while (fl != True):
        ma = e.pop(0)
        for i, o in enumerate(m):
            for j, mm in enumerate(o):
                if (mm <= ma):
                    m[i].remove(mm)
                    way[i].add(j)
        fl = True
        x = 0
        u = [0]
        if (step(0, 0, x) == False):
            fl = False
        else:
            fl = True
        if (fl == True):
            print(ma)
