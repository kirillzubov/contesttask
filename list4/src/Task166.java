import java.io.*;
import java.util.*;

public class Task166 {
    private static int newRobots(int n) {
        int max = 0;
        for (int n3 = 0; n3 <= 4 && n3 * 3 <= n; n3++) {
            int n5 = (n - n3 * 3) / 5;
            max = Math.max(max, n3 * 5 + n5 * 9);
        }
        return max;
    }

    public static void main(String[] args) {
        try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
            int k = in.nextInt();
            int n = in.nextInt();
            int[] eraOfRobs = new int[3];
            eraOfRobs[0] = k;

            for (int i = 1; i < n; i++) {
                int countR = eraOfRobs[0] + eraOfRobs[1] + eraOfRobs[2];
                eraOfRobs[i % 3] = newRobots(countR);
            }
            out.print(eraOfRobs[0] + eraOfRobs[1] + eraOfRobs[2]);
        }
    }
}