import java.io.*;
import java.util.*;

public class Task118 {
    static int joseph(int n, int k) {
        return n > 1 ? (joseph(n - 1, k) + k - 1) % n + 1 : 1;
    }

    public static void main(String[] args) {
        try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
            int n = in.nextInt();
            int k = in.nextInt();
            out.print(joseph(n, k));
        }
    }
}

