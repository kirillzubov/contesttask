import java.io.*;
import java.util.*;
public class Task330 {
    public static void main(String[] args) {
        try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
            int x1 = in.nextInt();int y1 = in.nextInt();
            int x2 = in.nextInt();int y2 = in.nextInt();
            if (Math.abs(x1 - x2) == Math.abs(y1 - y2)){
                out.println(1);
            }else if ((x1+y1+x2+y2)%2==0){
                out.println(2);
            }else {
                out.println(0);
            }
        }
    }
}
