import java.io.*;
import java.util.*;

public class Task121 {
    public static void main(String[] args) {
        try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
            int n = in.nextInt();
            int[] nails = new int[n];
            int[] a = new int[n];
            for (int i = 0; i < n; i++) {
                nails[i] = in.nextInt();
            }
            Arrays.sort(nails);
            a[1] = nails[1] - nails[0];
            if (2 < n) {
                a[2] = nails[2] - nails[0];
            }
            for (int i = 3; i < n; i++) {
                if (a[i - 1] < a[i - 2]) {
                    a[i] = a[i - 1] + nails[i] - nails[i - 1];
                } else {
                    a[i] = a[i - 2] + nails[i] - nails[i - 1];
                }
            }
            out.println(a[n - 1]);
        }
    }
}