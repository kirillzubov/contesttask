import math

R, k, r = map(int, input().split())

alfa = (2 * math.asin(r / (R - r))*180)/math.pi
f = math.pi * R * R * alfa / 360
s = math.pi * r * r * (180 - alfa) / 360
th = r * (R - r) * math.cos(alfa*math.pi/(2*180)) / 2
ms = f + s - 2*th
bs = math.pi*R*R
print(round(bs - k*ms,6))
