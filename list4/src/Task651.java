import java.io.*;
import java.util.*;

public class Task651 {
    private static List<Integer> getPrimesList(int a) {
        List<Integer> lis = new ArrayList<>();
        int i = 2;
        while (i <= a / i) {
            if (a % i == 0) {
                lis.add(i);
                a = a / i;
            } else {
                i++;
            }
        }
        if (a != 1) {
            lis.add(a);
        }
        return lis;
    }

    public static void main(String[] args) {
        try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
            int n = in.nextInt();
            int m = in.nextInt();
            List<Integer> a = getPrimesList(n);
            List<Integer> b = getPrimesList(m);
            int i = 0;
            int j = 0;
            int comFactCount = 0;
            while (i < a.size() && j < b.size()) {
                int comp = a.get(i) - b.get(j);
                if (comp == 0) {
                    comFactCount++;
                    i++;
                    j++;
                } else if (comp > 0) {
                    j++;
                } else {
                    i++;
                }
            }
            out.print(a.size() + b.size() - 2 * comFactCount);
        }
    }
}
