import java.io.*;
import java.util.*;

public class Task517 {
    public static void main(String[] args) {
        try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
            int count = in.nextInt();
            int[] keg = new int[count + 1];
            for (int i = 0; i < count; i++) {
                keg[i] = in.nextInt();
            }
            int sum = 0;
            int j = 0;
            int step = 0;
            while (step < 20) {
                if (keg[j] == 10) {
                    sum += keg[j] + keg[j + 1] + keg[j + 2];
                    j++;
                    step += 2;
                } else if (keg[j] + keg[j + 1] == 10 && step % 2 == 0) {
                    sum += keg[j] + keg[j + 1] + keg[j + 2];
                    j += 2;
                    step += 2;
                } else {
                    sum += keg[j];
                    j++;
                    step++;
                }
            }
            out.println(sum);
        }
    }
}
