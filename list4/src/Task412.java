import java.io.*;
import java.util.*;

public class Task412 {
    private static class Point {
        int x;
        int y;

        Point(int x, int y) {
            this.x = x;
            this.y = y;
        }
    }

    private static boolean between(int b1, int b2, int b0) {
        return (Math.min(b1, b2) < b0 && b0 < Math.max(b1, b2));
    }

    public static void main(String[] args) {
        try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
            Point[] p = new Point[3];
            for (int i = 0; i < 3; i++) {
                String st = in.next();
                p[i] = new Point(st.charAt(0) - 'a' + 1, st.charAt(1));
            }
            boolean isTheSameLine = (p[1].x - p[0].x) * (p[2].y - p[1].y) - (p[2].x - p[1].x) * (p[1].y - p[0].y) == 0;
            if ((p[1].x == p[2].x && (!isTheSameLine || !between(p[1].y, p[2].y, p[0].y)))
                    || (p[1].y == p[2].y && (!isTheSameLine || !between(p[1].x, p[2].x, p[0].x)))
                    || (Math.abs(p[1].y - p[2].y) == Math.abs(p[1].x - p[2].x) &&
                    (!isTheSameLine || !between(p[1].x, p[2].x, p[0].x)))) {
                out.print("YES");
            } else {
                out.print("NO");
            }
        }
    }
}