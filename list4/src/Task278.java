import java.io.*;
import java.util.*;

public class Task278 {
    public static void main(String[] args) {
        try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
            char [] s = in.nextLine().toCharArray();char [] t = in.nextLine().toCharArray();
            int j = 0;
            for (char c: t) {
                if (s[j] == c) {
                    j++;
                    if (j==s.length){
                        out.print("YES");
                        return;
                    }
                }
            }
            out.print("NO");
        }
    }
}