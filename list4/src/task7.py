n = int(input())
ma_x = [0]*(n+1)
qu = [0]
qu.extend(list(map(int, input().split())))
for i in range(1, n + 1):
    for j in range(1, i + 1):
        if (qu[i] % qu[j] == 0 ):
            if(i!=j):
                ma_x[i] = max(1 + ma_x[j],ma_x[i])
            else:
                ma_x[i] = max(ma_x[j],1)
m =0

print(max(ma_x))
