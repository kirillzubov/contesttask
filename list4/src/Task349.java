import java.io.*;
import java.util.*;

public class Task349 {
    public static void main(String[] args) {
        try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
            int fisrt = in.nextInt();
            int last = in.nextInt();
            boolean[] isPrime = new boolean[last + 1];
            Arrays.fill(isPrime, 2, isPrime.length, true);
            for (int i = 2; i*i <=last; i++) {
                if (isPrime[i]) {
                    for (int j = i * i; j <= last; j += i) {
                        isPrime[j] = false;
                    }
                }
            }
            boolean isAbsent = true;
            for (int i = fisrt; i <= last; i++) {
                if (isPrime[i]) {
                    isAbsent = false;
                    out.println(i);
                }
            }
            if (isAbsent) {
                out.print("Absent");
            }
        }
    }
}
