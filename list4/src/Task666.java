import java.io.*;
import java.util.*;

public class Task666 {
    public static void main(String[] args) {
        try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
            int n = (1 << 26) - in.nextInt();
            int deg = 0;
            int index = 1;
            while (index <= n) {
                index <<= 1;
                deg++;
            }
            index >>= 1;
            int i = 1;
            while (i < deg) {
                if (index == 0) break;
                n = n - index + 1;

                index >>= 1;
                while (index > n) {
                    index >>= 1;
                }
                i++;
            }
            out.print((char) (n - 1 + 'a'));
        }
    }
}