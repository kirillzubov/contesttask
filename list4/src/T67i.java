import java.io.*;
import java.util.*;

public class T67i {
    private static int readIp4(Scanner in) {
        int ip = 0;
        for (int i = 0; i < 4; i++) {
            ip = (ip << 8) | in.nextInt();
        }
        return ip;
    }

    public static void main(String[] args) {
        try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
            int nMasks = in.nextInt();
            in.useDelimiter("\\s+|\\.");
            int[] masks = new int[nMasks];
            for (int i = 0; i < nMasks; i++) {
                masks[i] = readIp4(in);
            }
            int nIps = in.nextInt();
            for (int i = 0; i < nIps; i++) {
                int count = 0;
                int ips1 = readIp4(in);
                int ips2 = readIp4(in);
                for (int k = 0; k < nMasks; k++) {
                    if ((masks[k] & ips1) == (masks[k] & ips2)) {
                        count++;
                    }
                }
                out.println(count);
            }
        }
    }
}
