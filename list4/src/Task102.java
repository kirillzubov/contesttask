import java.io.*;
import java.util.*;

public class Task102 {
    private static class Point {
        int x;
        int y;

        Point(int x, int y) {
            this.x = x;
            this.y = y;
        }

        private static Point getPoint(Scanner in) {
            return new Point(in.nextInt(), in.nextInt());
        }
    }

    private static int getDoubleAreaOfTriangle(Point a, Point b, Point c) {
        return Math.abs((a.x - c.x) * (b.y - c.y) - (b.x - c.x) * (a.y - c.y));
    }

    public static void main(String[] args) {
        try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
            Point p1 = Point.getPoint(in);
            Point p2 = Point.getPoint(in);
            Point p3 = Point.getPoint(in);
            Point p = Point.getPoint(in);
            int s = getDoubleAreaOfTriangle(p1, p2, p3);
            int a = getDoubleAreaOfTriangle(p, p2, p3);
            int b = getDoubleAreaOfTriangle(p1, p, p3);
            int c = getDoubleAreaOfTriangle(p, p1, p2);
            if (a + b + c == s) {
                out.print("In");
            } else {
                out.print("Out");
            }
        }
    }

}