import java.io.*;
import java.util.*;

public class Task408 {
    private static String addSpace(int n) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < n; i++) {
            sb.append(" ");
        }
        return sb.toString();
    }

    public static void main(String[] args) {
        try (Scanner in = new Scanner(new InputStreamReader(System.in, "cp866"));
             PrintWriter out = new PrintWriter(new OutputStreamWriter(System.out, "cp866"))) {
            StringBuilder sb = new StringBuilder();
            int k = in.nextInt();
            int n = in.nextInt();
            in.nextLine();
            for (int i = 0; i < n; i++) {
                String str = in.nextLine().trim();
                int len = str.length();
                if (k < len) {
                    out.print("Impossible.");
                    return;
                }
                sb.append(addSpace((k - len) / 2));
                sb.append(str);
                sb.append(addSpace((k - len) / 2 + (k - len) % 2));
                sb.append("\n");
            }
            out.print(sb);
        } catch (UnsupportedEncodingException e) {
            throw new Error();
        }
    }
}