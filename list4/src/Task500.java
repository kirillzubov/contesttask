import java.io.*;
import java.util.*;

public class Task500 {
    static class Agent {
        int age = 0;
        int risk = 0;

        Agent(int age, int risk) {
            this.age = age;
            this.risk = risk;
        }
    }

    public static void main(String[] args) {
        try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
            int n = in.nextInt();
            Agent[] agents = new Agent[n];
            int[] a = new int[n];
            for (int i = 0; i < n; i++) {
                agents[i] = new Agent(in.nextInt(), in.nextInt());
            }
            Arrays.sort(agents, Comparator.comparingInt(value -> value.age));
            a[1] = agents[1].risk;
            if (n > 2) {
                a[2] = agents[2].risk + agents[1].risk;
            }
            for (int i = 3; i < n; i++) {
                a[i] = Math.min(a[i - 1], a[i - 2]) + agents[i].risk;
            }
            out.println(a[n - 1]);
        }
    }
}