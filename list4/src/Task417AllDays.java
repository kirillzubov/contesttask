import java.io.*;
import java.text.*;
import java.util.*;

public class Task417AllDays {
    public static void main(String[] args) {
        try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
            int days = in.nextInt();
            String date = "01.01.2008";
            SimpleDateFormat inDateFormat= new SimpleDateFormat("dd.mm.yyyy");
            SimpleDateFormat outDateFormat = new SimpleDateFormat("EEEE, dd.MM");
            Calendar c = Calendar.getInstance();
            try {
                c.setTime(inDateFormat.parse(date));
            } catch (ParseException e) {
                e.printStackTrace();
            }
            c.add(Calendar.DATE, days);
            date = outDateFormat.format(c.getTime());
            out.println(date);
        }
    }
}