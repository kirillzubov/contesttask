first = input()
second = input()
lf = len(first)
ls = len(second)
m = 0
for j in range(ls-lf+1):
    local_max = 0
    for i in range(lf):
        if(first[i] == second[j+i]):
            local_max += 1
            m = max(local_max, m)
print(lf - m)
