import java.io.PrintWriter;
import java.util.Scanner;

public class Task664 {
    public static void main(String[] args) {
        try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
            int k = in.nextInt();
            int m = in.nextInt();
            int n = in.nextInt();
            int ans;
            int mod = n % k;
            if (n <= k) {
                ans = 2 * m;
            } else if (mod == 0) {
                ans = (n / k) * 2 * m;
            } else if (mod <= k / 2) {
                ans = (n / k) * 2 * m + m;
            } else {
                ans = (n / k) * 2 * m + 2 * m;
            }
            out.println(ans);
        }
    }
}
