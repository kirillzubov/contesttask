import java.util.*;
import java.io.*;

public class Task313 {
    public static void main(String[] args) {
        try (FastScanner in = new FastScanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
            int num = in.nextInt();
            int length = 101;

            int[] last = new int[length];
            int extrMax = 0;
            for (int i = 1; i <= num; i++) {
                int st = in.nextInt();
                if (last[st] != 0) {
                    extrMax = Math.max(extrMax, i - last[st]);
                }
                last[st] = i;
            }
            out.println(extrMax);
        }
    }

    static class FastScanner implements AutoCloseable {
        BufferedReader br;
        StringTokenizer st;

        FastScanner(InputStream stream) {
            try {
                br = new BufferedReader(new InputStreamReader(stream));
            } catch (Exception e) {
                throw new AssertionError(e);
            }
        }

        String next() {
            while (st == null || !st.hasMoreTokens()) {
                try {
                    st = new StringTokenizer(br.readLine());
                } catch (IOException e) {
                   throw  new AssertionError(e);
                }
            }
            return st.nextToken();
        }

        int nextInt() {
            try {
                return Integer.parseInt(next());
            } catch (Exception e) {
                throw  new AssertionError(e);
            }
        }

        @Override
        public void close()  {
            try {
                br.close();
            } catch (IOException e) {
                throw  new AssertionError(e);
            }
        }
    }
}
