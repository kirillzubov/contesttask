import java.io.*;
import java.util.*;

public class Task168 {
    public static void main(String[] args) {
        try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
            int n = in.nextInt();
            StringBuilder sb = new StringBuilder();
            int i = 1;
            while (i <= n) {
                sb.append(i);
                i++;
            }
            String str = sb.toString();
            out.println(str.indexOf(Integer.toString(n)) + 1);
        }
    }
}
