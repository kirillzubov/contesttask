import java.io.*;
import java.util.*;

public class Task69 {
    public static void main(String[] args) {
        try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
            double n = in.nextDouble();
            double a = in.nextDouble();
            double angle = Math.PI / n;
            double cos = Math.sin(angle);
            double tan = Math.tan(angle);
            double R = a / (cos * 2);
            double r = a / (tan * 2);
            double degreeQuality = R - r;
            if (degreeQuality < 1) {
                out.println("YES");
            } else {
                out.println("NO");
            }
        }
    }
}