import java.io.*;
import java.util.*;

public class Task399 {

    private static int findMin(int i, int j, int[][] map) {
        int min = Integer.MAX_VALUE;
        min = Math.min(map[i - 1][j], min);
        min = Math.min(map[i + 1][j], min);
        min = Math.min(map[i][j - 1], min);
        return Math.min(map[i][j + 1], min);
    }

    private static int getCountStep(int[][] map) {
        int i = 1;
        int j = 1;
        int count = 0;
        int di = 1;
        int dj = 0;
        int n = map.length;
        int m = map[0].length;

        while (!(i == n - 2 && j == m - 2)) {
            map[i][j] += 1;
            count++;

            if (count > 2000000) {
                return -1;
            }

            int min = findMin(i, j, map);
            if (min == Integer.MAX_VALUE) {
                count = -1;
                return count;
            }

            int nexti = i + di;
            int nextj = j + dj;
            if (map[nexti][nextj] == min && nexti < n - 1 && nextj < m - 1 && nexti >= 1 && nextj >= 1) {
                i = nexti;
                j = nextj;
            } else if (i < n - 1 && map[i + 1][j] == min) {
                di = 1;
                dj = 0;
                i = i + 1;
            } else if (j < m - 1 && map[i][j + 1] == min) {
                di = 0;
                dj = 1;
                j = j + 1;
            } else if (i > 1 && map[i - 1][j] == min) {
                di = -1;
                dj = 0;
                i = i - 1;
            } else if (j > 1 && map[i][j - 1] == min) {
                di = 0;
                dj = -1;
                j = j - 1;
            }
        }
        return count;
    }

    public static void main(String[] args) {

        try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
            int n = in.nextInt();
            int m = in.nextInt();
            int[][] map = new int[n][m];
            in.nextLine();
            for (int i = 0; i < n; i++) {
                String str = in.nextLine();
                for (int j = 0; j < m; j++) {
                    if ('@' == str.charAt(j)) {
                        map[i][j] = Integer.MAX_VALUE;
                    } else {
                        map[i][j] = 0;
                    }
                }
            }
            out.println(getCountStep(map));
        }
    }
}

