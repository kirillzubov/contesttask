import java.io.*;
import java.util.*;
public class Task528 {
    public static void main(String[] args) {
        try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
            long n = in.nextInt();long k = in.nextInt();
            out.println(((k*k+k-2)*(n-2) >> 1)+n + k - 1);
        }
    }
}
