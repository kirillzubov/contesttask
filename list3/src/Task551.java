import java.io.PrintWriter;
import java.util.Scanner;

public class Task551 {
    public static void main(String[] args) {
        try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
            int rb = in.nextInt();
            int r = in.nextInt();
            int h = in.nextInt();
            int b = in.nextInt();
            if (rb * rb - r * r >= (h + r - b) * (h + r - b) || (h + r - b) <= 0) {
                out.println("YES");
            } else {
                out.println("NO");
            }
        }
    }
}
