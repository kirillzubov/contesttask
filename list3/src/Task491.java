import java.io.*;
import java.util.*;

public class Task491 {

    private static boolean isPalindromicString(String word) {
        int i = 0;
        int j = word.length() - 1;

        while (i < j) {
            if (word.charAt(i) != word.charAt(j)) {
                return false;
            }
            i++;
            j--;
        }
        return true;
    }

    private static boolean areCharsTheSame(String str) {
        char same = str.charAt(0);
        for (int i = 1; i < str.length(); i++) {
            if (same != str.charAt(i)) {
                return false;
            }
        }
        return true;
    }


    public static void main(String[] args) {
        try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
            String str = in.next();

            if (areCharsTheSame(str)) {
                out.println("NO SOLUTION");
                return;
            }

            if (!isPalindromicString(str)) {
                out.println(str);
            } else {
                out.println(str.substring(1));
            }
        }
    }
}

