import java.io.*;

public class Task41 {
    public static void main(String[] args) {
        try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
            int[] temperature = new int[201];
            int n = in.nextInt();
            for (int i = 0; i < n; i++) {
                int index = in.nextInt() + 100;
                temperature[index] += 1;
            }
            int temp = 0;
            while (temp < 201) {
                while (temperature[temp] != 0) {
                    out.print(temp - 100 + " ");
                    temperature[temp]--;
                }
                temp++;
            }

        }
    }


    static class Assert {
        static void check(boolean e) {
            if (!e) {
                throw new AssertionError();
            }
        }
    }

    static class Scanner implements AutoCloseable {

        InputStream is;
        byte buffer[] = new byte[1 << 16];
        int size = 0;
        int pos = 0;

        Scanner(InputStream is) {
            this.is = is;
        }

        int nextChar() {
            if (pos >= size) {
                try {
                    size = is.read(buffer);
                } catch (IOException e) {
                    throw new IOError(e);
                }
                pos = 0;
                if (size == -1) {
                    return -1;
                }
            }
            Assert.check(pos < size);
            int c = buffer[pos] & 0xFF;
            pos++;
            return c;
        }

        int nextInt() {
            int c = nextChar();
            while (c == ' ' || c == '\r' || c == '\n' || c == '\t') {
                c = nextChar();
            }
            if (c == '-') {
                c = nextChar();
                Assert.check('0' <= c && c <= '9');
                int n = -(c - '0');
                c = nextChar();
                while ('0' <= c && c <= '9') {
                    int d = c - '0';
                    c = nextChar();
                    Assert.check(n > Integer.MIN_VALUE / 10 || n == Integer.MIN_VALUE / 10 && d <= -(Integer.MIN_VALUE % 10));
                    n = n * 10 - d;
                }
                Assert.check(c == -1 || c == ' ' || c == '\r' || c == '\n' || c == '\t');
                return n;
            } else {
                Assert.check('0' <= c && c <= '9');
                int n = c - '0';
                c = nextChar();
                while ('0' <= c && c <= '9') {
                    int d = c - '0';
                    c = nextChar();
                    Assert.check(n < Integer.MAX_VALUE / 10 || n == Integer.MAX_VALUE / 10 && d <= Integer.MAX_VALUE % 10);
                    n = n * 10 + d;
                }
                Assert.check(c == -1 || c == ' ' || c == '\r' || c == '\n' || c == '\t');
                return n;
            }
        }

        long nextLong() {
            int c = nextChar();
            while (c == ' ' || c == '\r' || c == '\n' || c == '\t') {
                c = nextChar();
            }
            if (c == '-') {
                c = nextChar();
                Assert.check('0' <= c && c <= '9');
                long n = -(c - '0');
                c = nextChar();
                while ('0' <= c && c <= '9') {
                    int d = c - '0';
                    c = nextChar();
                    Assert.check(n > Long.MIN_VALUE / 10 || n == Long.MIN_VALUE / 10 && d <= -(Long.MIN_VALUE % 10));
                    n = n * 10 - d;
                }
                Assert.check(c == -1 || c == ' ' || c == '\r' || c == '\n' || c == '\t');
                return n;
            } else {
                Assert.check('0' <= c && c <= '9');
                long n = c - '0';
                c = nextChar();
                while ('0' <= c && c <= '9') {
                    int d = c - '0';
                    c = nextChar();
                    Assert.check(n < Long.MAX_VALUE / 10 || n == Long.MAX_VALUE / 10 && d <= Long.MAX_VALUE % 10);
                    n = n * 10 + d;
                }
                Assert.check(c == -1 || c == ' ' || c == '\r' || c == '\n' || c == '\t');
                return n;
            }
        }

        @Override
        public void close() {
            try {
                is.close();
            } catch (IOException e) {
                throw new IOError(e);
            }
        }
    }
}