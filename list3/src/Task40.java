import java.io.*;
import java.util.*;

public class Task40 {

    public static void main(String[] args) {
        try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
            int degree = in.nextInt();
            int length = (int) (degree * Math.log10(2)) + 1;
            int[] num = new int[length];
            num[0] = 1;
            for (int j = 0; j < degree; j++) {
                int carry = 0;
                for (int i = 0; i < num.length; i++) {
                    carry = carry + num[i] * 2;
                    num[i] = carry % 10;
                    carry /= 10;
                }
            }
            for (int i = num.length - 1; i >= 0; i--) {
                out.print(num[i]);
            }
        }
    }
}

