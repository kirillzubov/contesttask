import java.io.*;
import java.util.*;

public class Task486 {

    private static long pow(long n, long degree){
        long ans =n;
        for (int i = 1; i <degree ; i++) {
            ans *=n;
        }
        return ans;
    }

    public static void main(String[] args) {
        try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
            long n = in.nextInt();
            long k = in.nextInt();
            out.println(pow(n, n) - (n - 1) * k);

        }
    }
}
