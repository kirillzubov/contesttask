import java.io.*;
import java.util.*;

public class Task273 {
    private static int getTrueCount(boolean[] was) {
        int countTrue = 0;
        for (boolean w : was) {
            if (w) {
                countTrue++;
            }
        }
        return countTrue;
    }

    public static void main(String[] args) {
        try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
            String n = in.next();
            int size = n.length();
            boolean[] was = new boolean[900];
            for (int i = 0; i < size; i++) {
                for (int j = i + 1; j < size; j++) {
                    for (int k = j + 1; k < size; k++) {
                        if (n.charAt(i) != '0') {
                            int num = (n.charAt(i) - '0') * 100 + (n.charAt(j) - '0') * 10 + (n.charAt(k) - '0') - 100;
                            was[num] = true;
                        }
                    }
                }
            }
            out.println(getTrueCount(was));
        }
    }
}
