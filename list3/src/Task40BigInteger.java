import java.io.*;
import java.math.BigInteger;
import java.util.*;

public class Task40BigInteger {

    public static void main(String[] args) {
        try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
            int degree = in.nextInt();
            out.println(BigInteger.ONE.shiftLeft(degree));
        }
    }
}

