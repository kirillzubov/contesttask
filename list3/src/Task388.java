import java.io.*;
import java.util.*;

public class Task388 {
    public static void main(String[] args)  {
        try (FastScanner in = new FastScanner(System.in); PrintWriter out = new PrintWriter(System.out)){

            int n = in.nextInt();
            int m = in.nextInt();

            int matrix[][] = new int[n][m];
            int minRows[] = new int[n];
            int maxColumns[] = new int[m];
            for (int i = 0; i < n; i++) {
                minRows[i] = Integer.MAX_VALUE;
            }
            for (int i = 0; i < m; i++) {
                maxColumns[i] = Integer.MIN_VALUE;
            }

            for (int i = 0; i < n; i++) {
                for (int j = 0; j < m; j++) {
                    matrix[i][j] = in.nextInt();
                    if (minRows[i] > matrix[i][j]) {
                        minRows[i] = matrix[i][j];
                    }
                    if (maxColumns[j] < matrix[i][j]) {
                        maxColumns[j] = matrix[i][j];
                    }
                }
            }

            int count = 0;
            for (int i = 0; i < n; i++) {
                for (int j = 0; j < m; j++) {
                    if (minRows[i] == maxColumns[j]) {
                        count += 1;
                    }
                }
            }
            out.println(count);
        }
    }


    static class FastScanner implements AutoCloseable {
        BufferedReader br;
        StringTokenizer st;

        FastScanner(InputStream stream) {
            try {
                br = new BufferedReader(new InputStreamReader(stream));
            } catch (Exception e) {
                throw new AssertionError(e);
            }
        }

        String next() {
            while (st == null || !st.hasMoreTokens()) {
                try {
                    st = new StringTokenizer(br.readLine());
                } catch (IOException e) {
                    throw new AssertionError(e);
                }
            }
            return st.nextToken();
        }

        int nextInt() {
            try {
                return Integer.parseInt(next());
            } catch (Exception e) {
                throw  new AssertionError(e);
            }
        }

        @Override
        public void close() {
            try {
                br.close();
            } catch (IOException e) {
                throw new AssertionError(e);
            }
        }
    }
}

