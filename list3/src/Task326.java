import java.io.*;
import java.util.*;

public class Task326 {
    public static void main(String[] args) {
        try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
            int n = in.nextInt();
            int[] count = new int[201];
            int[] nums = new int[n];
            for (int i = 0; i < n; i++) {
                nums[i] = in.nextInt();
            }
            for (int i = 0; i < n; i++) {
                count[nums[i] + 100]++;
            }
            int maxCount = Integer.MIN_VALUE;
            int index = 0;
            for (int i = 0; i < 201; i++) {
                if (count[i] > maxCount) {
                    maxCount = count[i];
                    index = i;
                }
            }
            index -= 100;
            for (int i = 0; i < n; i++) {
                if (nums[i] != index) {
                    out.print(nums[i]+" ");
                }
            }
            for (int i = 0; i <maxCount-1 ; i++) {
                out.print(index+" ");

            }
            out.print(index);
        }
    }
}
