import java.io.*;
import java.util.*;

public class Task173 {
    private static String transIntoNotation(int num, int notation) {
        StringBuilder sb = new StringBuilder();
        while (num != 0) {
            int digit = num % notation;
            if (digit >= 10) {
                sb.append((char) (digit - 10 + 'A'));
            } else {
                sb.append(digit);
            }
            num /= notation;
        }
        return sb.toString();
    }

    private static boolean isPalindromicString(String word) {
        int i = 0;
        int j = word.length() - 1;

        while (i < j) {
            if (word.charAt(i) != word.charAt(j)) {
                return false;
            }
            i++;
            j--;
        }
        return true;
    }

    public static void main(String[] args) {
        try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
            int n = in.nextInt();

            List<Integer> ansNotations = new ArrayList<>();

            for (int notation = 2; notation <= 36; notation++) {
                String numInNatation = transIntoNotation(n, notation);
                if (isPalindromicString(numInNatation)) {
                    ansNotations.add(notation);
                }
            }

            if (ansNotations.isEmpty()) {
                out.println("none");
            } else if (ansNotations.size() == 1) {
                out.println("unique");
            } else {
                out.println("multiple");
            }

            for (int not : ansNotations) {
                out.print(not + " ");
            }
        }
    }
}
