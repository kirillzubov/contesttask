
import java.io.*;
import java.util.*;

public class Task295 {
    private static String cyclicShift(String str) {
        char[] chars = str.toCharArray();
        for (int i = 0; i < chars.length; i++) {
            chars[i] -= 1;
            if (chars[i] < 'A') {
                chars[i] += 26;
            }
        }
        return String.valueOf(chars);
    }

    public static void main(String[] args) {
        try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
            String chars = in.next();
            String word = in.next();
            for (int i = 0; i <= 25; i++) {
                if (chars.contains(word)) {
                    out.println(chars);
                    return;
                }
                chars = cyclicShift(chars);
            }
            out.println("IMPOSSIBLE");
        }
    }
}
