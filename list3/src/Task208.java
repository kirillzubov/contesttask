import java.io.PrintWriter;
import java.util.Scanner;

public class Task208 {
    private static String cyclicShift(String str) {
        return str.substring(1) + str.charAt(0);
    }

    public static void main(String[] args) {
        try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
            long n = in.nextInt();
            String str = Long.toBinaryString(n);
            int length = str.length();
            int i = 0;
            long max = Long.MIN_VALUE;
            while (length>0) {
                str = cyclicShift(str);
                max = Math.max(Long.valueOf(str,2), max);
                length--;
            }
            out.println(max);
        }
    }
}
