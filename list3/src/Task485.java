import java.io.*;
import java.util.*;

public class Task485 {
    private static boolean isTheCountFish(int num, int n, int k) {
        int i = 0;
        while (i < n) {
            if (num % n == k) {
                num /= n;
                num *= (n - 1);
            } else {
                return false;
            }
            i++;
        }
        return true;
    }

    public static void main(String[] args) {
        try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
            int n = in.nextInt();
            int k = in.nextInt();
            int i = 1;
            while (true) {
                int countFish = i * n + k;
                if (isTheCountFish(countFish, n, k)) {
                    out.println(countFish);
                    return;
                }
                i++;
            }
        }
    }
}
