import java.io.*;
import java.util.*;

public class Task208B {
    private static int getLength(int n) {
        int length =0 ;
        while (n!=0){
            n/=2;
            length++;
        }
        return length;
    }

    public static void main(String[] args) {
        try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
            int n = in.nextInt();
            int max = Integer.MIN_VALUE;
            int len = getLength(n);
            int i = 0;

            if (n == 0) {
                out.println(0);
                return;
            }
            while (i < len) {
                int bit = n % 2;
                bit = bit << len-1;
                n = n >> 1;
                n = n + bit;
                max = Math.max(n, max);
                i++;
            }
            out.println(max);
        }
    }
}
