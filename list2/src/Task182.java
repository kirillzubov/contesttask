import java.io.PrintWriter;
import java.util.Scanner;

public class Task182 {
    public static void main(String[] args) {
        try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
            int x1 = in.nextInt();
            int y1 = in.nextInt();

            int x2 = in.nextInt();
            int y2 = in.nextInt();

            int x3 = in.nextInt();
            int y3 = in.nextInt();

            int x,y;

            int d12 = (x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2);
            int d13 = (x1 - x3) * (x1 - x3) + (y1 - y3) * (y1 - y3);
            int d23 = (x3 - x2) * (x3 - x2) + (y3 - y2) * (y3 - y2);

            if (d12 + d23 == d13) {
                x = x1 + x3 - x2;
                y = y1 + y3 - y2;
            } else if (d13 + d23 == d12) {
                x = x1 + x2 - x3;
                y = y1 + y2 - y3;
            } else if (d13 + d12 == d23) {
                x = x2 + x3 - x1;
                y = y2 + y3 - y1;
            } else {
                throw new AssertionError();
            }
            out.print(x + " " + y);
        }
    }
}
