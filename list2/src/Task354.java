import java.io.PrintWriter;
import java.util.Scanner;

public class Task354 {

    public static void main(String[] args) {
        try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
            int a = in.nextInt();
            int i = 2;
            while (i <= a / i) {
                if (a % i == 0) {
                    out.print(i + "*");
                    a = a / i;
                } else {
                    i++;
                }
            }
            out.print(a);
        }
    }
}
