import java.io.*;
import java.util.*;

public class Task520 {
    public static void main(String[] args) {
        try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
            int countOfPairOfSocks = in.nextInt();
            int boxes = countOfPairOfSocks / 144;

            countOfPairOfSocks %= 144;
            int bundles = countOfPairOfSocks / 12;

            countOfPairOfSocks %= 12;
            int pairs = countOfPairOfSocks;

            if (pairs * 105 > 1025) {
                bundles++;
                pairs = 0;
            }
            if (bundles * 1025 > 11400) {
                boxes++;
                bundles = 0;
                pairs = 0;
            }
            if (11400 < bundles * 1025 + pairs * 105) {
                boxes++;
                bundles = 0;
                pairs = 0;
            }
            out.println(boxes + " " + bundles + " " + pairs);
        }
    }
}
