import java.io.*;
import java.util.*;

public class Task88 {

    private static boolean areRowsValid(boolean[] wasNum, int[][] sudoku) {
        int n2 = sudoku.length;
        for (int i = 0; i < n2; i++) {
            Arrays.fill(wasNum, false);
            for (int j = 0; j < n2; j++) {
                if (sudoku[i][j] > n2) {
                    return false;
                }
                if (wasNum[sudoku[i][j] - 1]) {
                    return false;
                }
                if (!wasNum[sudoku[i][j] - 1]) {
                    wasNum[sudoku[i][j] - 1] = true;
                }
            }
        }
        return true;
    }

    private static boolean areColumnsValid(boolean[] wasNum, int[][] sudoku) {
        int n2 = sudoku.length;
        for (int j = 0; j < n2; j++) {
            Arrays.fill(wasNum, false);
            for (int i = 0; i < n2; i++) {
                if (sudoku[i][j] > n2) {
                    return false;
                }
                if (wasNum[sudoku[i][j] - 1]) {
                    return false;
                }
                if (!wasNum[sudoku[i][j] - 1]) {
                    wasNum[sudoku[i][j] - 1] = true;
                }
            }
        }
        return true;
    }

    private static boolean areMeanSquaresValid(boolean[] wasNum, int[][] sudoku, int n) {
        int n2 = sudoku.length;
        for (int jj = 0; jj < n2; jj += n) {
            for (int ii = 0; ii < n2; ii += n) {
                Arrays.fill(wasNum, false);
                for (int i = ii; i < n + ii; i++) {
                    for (int j = jj; j < n + jj; j++) {
                        if (sudoku[i][j] > n2) {
                            return false;
                        }
                        if (wasNum[sudoku[i][j] - 1]) {
                            return false;
                        }
                        if (!wasNum[sudoku[i][j] - 1]) {
                            wasNum[sudoku[i][j] - 1] = true;
                        }
                    }
                }
            }
        }
        return true;
    }

    public static void main(String[] args) {
        try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
            int n = in.nextInt();
            int n2 = n * n;

            boolean wasNum[] = new boolean[n2];
            int sudoku[][] = new int[n2][n2];

            for (int i = 0; i < sudoku.length; i++) {
                for (int j = 0; j < sudoku[i].length; j++) {
                    sudoku[i][j] = in.nextInt();
                }
            }
            if (areRowsValid(wasNum, sudoku) && areMeanSquaresValid(wasNum, sudoku, n) && areColumnsValid(wasNum, sudoku)) {
                out.println("Correct");
            } else {
                out.println("Incorrect");
            }
        }
    }
}
