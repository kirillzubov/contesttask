import java.io.*;
import java.util.*;

public class Task684 {
    private static boolean isBlack(int h, int v) {
        return (h + v) % 2 == 0;
    }

    private static boolean isMovePossible(int h1, int v1, int h2, int v2) {
        if (!isBlack(h2, v2)) {
            return false;
        } else if (v2 <= v1) {
            return false;
        } else if (Math.abs(h2 - h1) > v2 - v1) {
            return false;
        }
        return true;
    }


    public static void main(String[] args) {
        try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {

            String startCell = in.next();
            String finishCell = in.next();

            int h1 = startCell.charAt(0) - 'a' + 1;
            int v1 = startCell.charAt(1) - '0';
            int h2 = finishCell.charAt(0) - 'a' + 1;
            int v2 = finishCell.charAt(1) - '0';

            if (isMovePossible(h1, v1, h2, v2)) {
                out.println("YES");
            } else {
                out.println("NO");
            }
        }
    }
}
