import java.io.*;
import java.util.*;

public class Task383 {
    private static boolean isMagicNum(int n) {
        int sum = 0;
        int count = 0;
        while (n != 0) {
            sum += n % 10;
            n /= 10;
            count += 1;
        }
        return sum % count == 0;
    }

    public static void main(String[] args) {
        try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
            int num = in.nextInt();
            int magicNumCount = 0;
            int n = 1;
            while (true) {
                if (isMagicNum(n)) {
                    magicNumCount += 1;
                }
                if (magicNumCount == num) {
                    out.println(n);
                    return;
                }
                n += 1;
            }
        }
    }
}
