import java.io.PrintWriter;
import java.util.Scanner;

public class Task250 {

    private static boolean isTwofold(int num) {
        int a = num % 10;
        num /= 10;
        int b = a;

        while (num != 0) {
            int digit = num % 10;
            num /= 10;
            if (a == b && (a != digit)) {
                b = digit;
            }
            if (a != b && (a != digit && b != digit)) {
                return false;
            }
        }
        return true;
    }

    public static void main(String[] args) {
        try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
            int n = in.nextInt();
            for (int d = 0; ; d++) {
                if (isTwofold(n - d)) {
                    out.println(n - d);
                    return;
                }
                if (isTwofold(n + d)) {
                    out.println(n + d);
                    return;
                }
            }
        }
    }
}