import java.io.*;
import java.util.*;

public class Task317 {
    public static void main(String[] args) {
        try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {

            int x = in.nextInt();
            int y = in.nextInt();
            int z = in.nextInt();
            int w = in.nextInt();

            int ans = 0;
            for (int i = 0; i <= w / x; i++) {
                for (int j = 0; j <= w / y; j++) {
                    int cur = w - (x * i + y * j);
                    if (cur < 0) break;
                    if (cur % z == 0) ans++;
                }
            }
            out.println(ans);
        }
    }
}