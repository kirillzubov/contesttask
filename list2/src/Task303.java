import java.io.*;
import java.util.*;

public class Task303 {
    public static void main(String[] args) {
        try (Scanner in = new Scanner(System.in); PrintWriter out = new PrintWriter(System.out)) {
            String n = in.next();
            int max = Integer.MIN_VALUE;
            for (int j = 0; j < n.length(); j++) {
                int sum = 0;
                int sign = 1;
                for (int i = 0; i < j; i++) {
                    sum += (n.charAt(i) - '0') * sign;
                    sign *= -1;
                }
                for (int i = j + 1; i < n.length(); i++) {
                    sum += (n.charAt(i) - '0') * sign;
                    sign *= -1;
                }
                max = Math.max(max, sum);
            }
            out.println(max);
        }
    }
}
