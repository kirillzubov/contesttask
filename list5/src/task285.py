n, m = map(int, input().split())
time_list = [int(x) for x in input().split()]
for t in time_list:
    if t > m:
        print('no')
        exit()
sum_time = 0
sum_time = sum(time_list) - n + 1
print( m <= sum_time and 'yes' or 'no')
