N, M = map(int, input().split())
count = 0
if N % 2 != 0 and M == 0:
    print(-1)
else:
    while M % 2 != 0 or (M // 2 + N) % 2!=0:
        M += 1
        count+=1
    while M>0:
        M-=2
        N+=1
        count+=1
    while N>0:
        N -= 2
        count += 1
    print(count)
